// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

function toggle(display) {
  let cmd = `
document.querySelector('.free-board-info').style.display = "${display}";
document.querySelector('.toolbar2').style.display = "${display}";
document.querySelector('#main').style.display = "${display}";
document.querySelector('.toolbar').style.display = "${display}";
document.querySelector('.library-close').style.display = "${display}";
document.querySelector('.library-selection').style.display = "${display}";
document.querySelector('.scaleSlider').style.display = "${display}";
document.querySelector('.library').style.display = "${display}";
document.querySelector('.document-state').style.display = "${display}";
`;
  return cmd;
}
document.getElementById('hideToolbars').onclick = function(element) {
  // let color = element.target.value;
  let cmd = toggle('none');
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(
        tabs[0].id,
      { code: cmd});
  });
};
document.getElementById('showToolbars').onclick = function (element) {
  // let color = element.target.value;
  let cmd = toggle('block');
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.executeScript(
      tabs[0].id,
      { code: cmd });
  });
};
